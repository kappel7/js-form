# A Contact Form for Weekly Newsletter

## What This App is About

This is a small JavaScript form validation project using PHP for backend.

## Tools used

JavaScript, PHP, MAMP, phpMyadmin

## Overview

The project includes one page with a form and another that displays a welcome message after the data has been submitted to the database. 