<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" crossorigin="anonymous" />
    <title>Document</title>
</head>

<?php
$firstName = $_POST["fName"];
$lastName = $_POST["lName"];
$email = $_POST["email"];
$password = $_POST["password"];

$conn = new mysqli('localhost', 'root', 'root', 'form_validation');
if ($conn->connect_error) {
    die('Connection failed : ' . $conn->connect_error);
    echo "Registration failed";
} else {
    $stmt = $conn->prepare("INSERT INTO registration(firstName, lastName, email, password) values(?,?,?,?)");
    $stmt->bind_param("ssss", $firstName, $lastName, $email, $password);
    $stmt->execute();
    $stmt->close();
    $conn->close();
}
?>

<body>
    <div class="successMessage">
        <div class="content">
            <div class="logo">
                <img src="https://svgshare.com/i/_go.svg" alt="" />
            </div>
            <div class="image"></div>
        </div>
        <div class="registration title">Welcome, <?php echo $_POST["fName"]; ?>!<br><br>
            You have registered the following email address: <?php echo $_POST["email"]; ?><br><br>
            Make sure to keep your passwords safe!
        </div>

    </div>
</body>

</html>