let id = (id) => document.getElementById(id);
let classes = (classes) => document.getElementsByClassName(classes);

let firstName = id("fName"),
  lastName = id("lName"),
  email = id("email"),
  password = id("password"),
  form = id("form"),
  errorMsg = classes("error"),
  successIcon = classes("success-icon"),
  failureIcon = classes("failure-icon");

const validateForm = (e) => {
  validator(firstName, 0, "First name cannot be blank!", e);
  validator(lastName, 1, "Last name cannot be blank!", e);
  validator(email, 2, "Please Enter a Valid e-mail Address!", e);
  validator(password, 3, "Password cannot be blank!", e);
};

let validator = (id, serial, message, e) => {
  if (id.value.trim() === "") {
    e.preventDefault();
    errorMsg[serial].innerHTML = message;
    id.style.border = "2px solid red";
    failureIcon[serial].style.opacity = "1";
    successIcon[serial].style.opacity = "0";

  } else if (serial === 2 && !id.value.includes("@")) {
    e.preventDefault();
    errorMsg[serial].innerHTML = message;
    id.style.border = "2px solid red";
    failureIcon[serial].style.opacity = "1";
    successIcon[serial].style.opacity = "0";

  } else {
    errorMsg[serial].innerHTML = "";
    id.style.border = "2px solid green";
    failureIcon[serial].style.opacity = "0";
    successIcon[serial].style.opacity = "1";
  }
};

const resetForm = (e) => {
  e.preventDefault();
  form.reset();
};
